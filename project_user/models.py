from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager


class CustomUser(AbstractUser):
    # Users Groups are managed via the Admin
    #     USER_LEVEL = (
    #     ("50", "Manager"),
    #     ("20", "Reporter"),
    #     ("30", "Developer"),
    # )
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email
