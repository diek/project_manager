## Basic project manager m2m  
### There are 3 users already in the database, a superuser, and 2 others.  

1. barney@rubble.com  
2. fred@flinstone.com  
The password is the same for each, YwxDttkYL4u9Uft  

## Project ERD  

![Entity Relationship Diagram](erd.png)
