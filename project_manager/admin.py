from django.contrib import admin

from .models import Project, RequiredPermission


class ProjectAdmin(admin.ModelAdmin):
    fields = [
        "project_name",
        "project_description",
        "project_url",
        "expires_at",
        "project_creator",
        "visibility_level",
        "status"
    ]


admin.site.register(Project, ProjectAdmin)


class RequiredPermissionAdmin(admin.ModelAdmin):
    fields = [
        "project",
        "employee",
        "can_access",
        "permission_expires_at",
        "updated_by"
    ]


admin.site.register(RequiredPermission, RequiredPermissionAdmin)
