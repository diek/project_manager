from config import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from multiselectfield import MultiSelectField
from project_user.models import CustomUser


class Project(models.Model):
    class ProjectStatus(models.TextChoices):
        NOT_STARTED = "N", _("Not Started")
        PENDING = "P", _("Pending")
        OUT_FOR_DELIVERY = "O", _("Out for delivery")
        DELIVERED = (
            "D",
            _("Delivered"),
        )

    LEVEL = (
        ("Level_1", "Public"),
        ("Level_2", "Private"),
        ("Level_3", "Internel"),
    )

    project_name = models.CharField(max_length=100)
    project_description = models.TextField()
    project_url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    expires_at = models.DateTimeField(
        default=timezone.now() + timezone.timedelta(days=10)
    )
    project_creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name="project_creator"
    )
    visibility_level = MultiSelectField(choices=LEVEL, default=LEVEL[0])
    status = models.CharField(
        max_length=2, choices=ProjectStatus.choices, default=ProjectStatus.NOT_STARTED
    )

    project_permissions = models.ManyToManyField(
        CustomUser, through="RequiredPermission", through_fields=("project", "employee")
    )

    def __str__(self):
        return self.project_name


class RequiredPermission(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.PROTECT, verbose_name="Projects"
    )
    employee = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name="employee"
    )
    # This is the critical field, can they access the project, yes or no
    can_access = models.BooleanField(default=False)
    permission_expires_at = models.DateTimeField(
        default=timezone.now() + timezone.timedelta(days=20)
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="manager",
    )

    def save_model(self, request, obj, form, change):
        obj.updated_by = request.user
        super().save_model(request, obj, form, change)

    def __str__(self):
        return f"{self.project}    {self.employee}     {self.can_access}"
