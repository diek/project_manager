from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import ListView


class ProjectListView(PermissionRequiredMixin, ListView):
    template_name = 'project_manager/projects.html'
    permission_required = ('polls.view_choice', 'polls.change_choice')
    permission_denied_message = 'You are not allowed to view this'
